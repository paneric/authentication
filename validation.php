<?php

declare(strict_types=1);

use Paneric\Authentication\DBAL\CredentialDTO;

return [
    'validation' => [
        'authe.credential.add' => [
            'methods' => ['POST'],
            CredentialDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                        'is_valid_email' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'password_hash' => [
                        'required' => [],
                        'has_min_length' => [8],
                        'is_valid_password' => [],
                    ],
                    'password_hash_repeat' => [
                        'is_same_as' => ['password_hash'],
                    ],
                    'role_id' => [
                        'required' => [],
                        'is_integer' => [],
                    ],
                    'terms' => [
                        'required' => [],
                        'is_one_of' => ['OK'],
                    ],
                ],
            ],
        ],
        'authe.credential.edit' => [
            'methods' => ['POST'],
            CredentialDTO::class => [
                'rules' => [
                    'role_id' => [
                        'required' => [],
                        'is_integer' => [],
                    ],
                ],
            ],
        ],
        'authe.credential.delete' => [
            'methods' => ['POST'],
            CredentialDTO::class => [
                'rules' => [],
            ],
        ],
    ],
];
