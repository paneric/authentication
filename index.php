<?php

declare(strict_types=1);

use Paneric\Authentication\AuthenticationController;
use Paneric\Authentication\Service\AuthenticationService;
use Paneric\Authentication\Service\CredentialService;
use Paneric\Authentication\Service\MailerService;
use Paneric\Doctrine\EntityManagerBuilder;
use Paneric\Mailer\MailerFactory;
use Paneric\Session\SessionWrapper;
use Paneric\Guard\Guard;
use Paneric\Twig\TwigBuilder;

define('ROOT_FOLDER', __DIR__ . '/');
define('APP_FOLDER', ROOT_FOLDER . 'src/');

require 'vendor/autoload.php';

$sessionConfig = require 'session-config.php';
$guardConfig = require 'guard-config.php';
$credentialServiceConfig = require 'credential-service-config.php';
$mailerConfig = require 'mailer-config.php';
$mailerServiceConfig = require 'mailer-service-config.php';
$twigConfig = require 'twig-config.php';

$randomFactory = new RandomLib\Factory;
$randomGenerator = $randomFactory->getMediumStrengthGenerator();

$guard = new Guard($randomGenerator, $guardConfig);

$mailerFactory = new MailerFactory();
$phpMailer = $mailerFactory->create($mailerConfig);

$twigBuilder = new TwigBuilder();
$twig = $twigBuilder->build($twigConfig);

$authenticationController = new AuthenticationController($authenticationService, $twig, $session);
