# Authentication

Authentication package. Very straightforward, but things become interesting when you take the whole "menu".

In the simplest approach, the package consists of three services depending on three interfaces, requiring slight support
of phpmailer and simple configuration.

For a little bit more detailed integration, take a look at delivered within the package supplements like middleware, 
controller, routes, translation file example, Twig templates forms with csrf protection and recaptcha implementation ...

## Required

* PHP 7.1+

## File structure
* **authentication**
    * **sql**
        * credentials.sql *(sql script for credentials table creation)*
    * **src**
        * **ORM**  *(optional - ORM Doctrine support classes)*
            * Credential.php
            * CredentialRepository.php
            * ORMRepository.php
        * **PDO**  *(optional - PDO Paneric support classes)*
            * CredentialRepository.php
            * PDORepository.php 
        * **Service**
            * AuthenticationService.php
            * CredentialService.php
            * MailerService.php
        * **templates** *(optional - set of propositions for Twig form templates)*
            * ...        
        * AuthenticationController.php *(optional)*
        * AuthenticationMiddleware.php *(optional)*
        * ClientController.php *(optional - base controller)*
        * CredentialDTO.php
        * CredentialRepositoryInterface.php
        * RecaptchaV3Middleware.php *(optional - captcha validator for reCaptcha v3 integration)*
    * ... (few useful configuration examples and some other staff)              

### composer
```sh
composer require paneric/authentication
```

## 1. Integration

### 1.1 Basic integration interfaces:

In this case, first, you need to prepare yourself three adapters of your favorites tools related to database layer, 
session and security, implementing the following interfaces:

*  CredentialRepositoryInterface.php *(delivered with the package)*
*  [SessionInterface.php](https://bitbucket.org/paneric/interfaces-Session)
*  [GuardInterface.php](https://bitbucket.org/paneric/interfaces-guard)

In relation to the first one, ORM Doctrine and PDO Paneric adapters examples are delivered by default if you managed to 
check the file structure with a bit of attention. To serve them, use your favorites solutions or consider:

*  [paneric/pdo-wrapper](https://bitbucket.org/paneric/pdo-wrapper) or [paneric/doctrine-builder](https://bitbucket.org/paneric/pdo-wrapper)

For the rest you can alternatively give a try for Paneric packages:

*  [paneric/session-wrapper](https://bitbucket.org/paneric/session-wrapper)
*  [paneric/guard](https://bitbucket.org/paneric/interfaces-guard)

### 1.2 Basic integration configuration: (PHP-DI example)

CredentialService.php
```php
    return [
        'credential_service_config' => [
            'role_id' => 3,
            'csrf_hash_field_name' => 'csrf_hash',
        ],
    ];
```
* **role_id**: this is an integer value of your base user role identity, required by implemented authorization solution in
your project.
* **csrf_hash_field_name**: this is a name of your forms csrf hash field.

MailerService.php 
```php
    return [
        'mailer_service_config' => [
            'random_string_length' => 128,

            'activation_url' => 'http://127.0.0.1:8080/authe',
            'activation_time_delay' => 24,

            'reset_url' => 'http://127.0.0.1:8080/authe',
            'reset_time_delay' => 24,

            'mail_content' => [
                'activation_email_subject' => [
                    'en' => 'Account activation automatic message.',
                    '...' => '',
                ],
                'activation_email_message' => [
                    'en' => 'Please click the following link within %sh to activate your account: %s/%s/%s/activate .',
                    '...' => '',
                ],
                'reset_email_subject' => [
                    'en' => 'Password reset automatic message.',
                    '...' => '',
                ],
                'reset_email_message' => [
                    'en' => 'Please click the following link within %sh to reset your account password: %s/%s/%s/reset .',
                    '...' => '',
                ],
            ],
        ],
    ];
```

### 1.2 Basic integration instantiation: (PHP-DI example)
```php
return [
    MailerService::class => function (Container $container): MailerService {
        return new MailerService(
            $container->get(SessionInterface::class),
            $container->get(GuardInterface::class),
            $container->get('php_mailer'),
            $container->get('mailer_service_config'),
            (string) $container->get('local')
        );
    },

    CredentialService::class => function (Container $container): CredentialService {
        return new CredentialService(
            $container->get(CredentialRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(GuardInterface::class),
            $container->get('credential_service_config')
        );
    },

    AuthenticationService::class => function (Container $container): AuthenticationService {
        return new AuthenticationService(
            $container->get(MailerService::class),
            $container->get(CredentialService::class),
            $container->get(GuardInterface::class),
            $container->get(SessionInterface::class)
        );
    },
];
```
## Final notice.

Now the only thing that needs to be done is inserting the AuthenticationService into your controller.
As the example approach or even final integration take a look as mentioned before at AuthenticationController, routes 
and form templates that present csrf, recaptcha, translation, flash and validation messages integration.

## Last but not least.

CredentialService requires validation information inserted into request as follows:
```php
$validation = $request->getAttribute('validation');

$validation = [
    CredentialDTO::class => [
        'ref' => [
            'ref error message 1',
            '...'
        ],
        'password_hash' => [
            'password_hash error message 1',
            '...'
        ],
    ],
];

// or empty if there is no validation errors detected:

$validation = [
    CredentialDTO::class => [],
];
```
