<?php

return [
    'templates_dirs' => [
        'authentication' => ROOT_FOLDER . 'src/templates'
    ],
    'options' => [
        'debug' => true, /* "prod" false */
        'charset' => 'UTF-8',
        'strict_variables' => false,
        'autoescape' => 'html',
        'cache' => false, /* "prod" ROOT_FOLDER.'/var/cache/twig'*/
        'auto_reload' => null,
        'optimizations' => -1,
    ],
];
