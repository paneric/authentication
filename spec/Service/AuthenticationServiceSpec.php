<?php

namespace spec\Paneric\Authentication\Service;

use DateTimeImmutable;
use Paneric\Authentication\DBAL\CredentialDTO;
use Paneric\Authentication\Service\AuthenticationService;
use Paneric\Authentication\Service\CredentialService;
use Paneric\Authentication\Service\MailerService;
use Paneric\Interfaces\DataObject\DataObjectInterface;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\Interfaces\Hydrator\HydratorInterface;
use Paneric\Interfaces\Session\SessionInterface;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Http\Message\ServerRequestInterface as Request;

class AuthenticationServiceSpec extends ObjectBehavior
{
    public function let(
        MailerService $mailerService,
        CredentialService $credentialService,
        GuardInterface $guard,
        SessionInterface $session
    ): void {
        $this->beConstructedWith($mailerService, $credentialService, $guard, $session);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(AuthenticationService::class);
    }

    public function it_creates_credential_1(
        Request $request,
        CredentialService $credentialService
    ): void {
        $request->getMethod()->willReturn('GET');

        $credentialService->create(
            Argument::type(Request::class),
            Argument::type('bool'),
            null,
            null
        )->willReturn([]);

        $this->createCredential($request)->shouldBeArray();
    }

    public function it_creates_credential_2(
        Request $request,
        GuardInterface $guard,
        MailerService $mailerService,
        CredentialService $credentialService
    ): void {
        $request->getMethod()->willReturn('POST');
        $guard->setUniqueId()->willReturn('id');
        $mailerService->setProcessId()->willReturn('activation_id');

        $credentialService->create(
            Argument::type(Request::class),
            Argument::type('bool'),
            Argument::type('string'),
            Argument::type('string')
        )->willReturn([]);

        $this->createCredential($request, true)->shouldBeArray();
    }

    public function it_creates_credential_3(
        Request $request,
        GuardInterface $guard,
        MailerService $mailerService,
        CredentialService $credentialService
    ): void {
        $request->getMethod()->willReturn('POST');
        $guard->setUniqueId()->willReturn('id');
        $mailerService->setProcessId()->willReturn('activation_id');

        $credentialService->create(
            Argument::type(Request::class),
            Argument::type('bool'),
            Argument::type('string'),
            Argument::type('string')
        )->willReturn(null);

        $credentialService->getRef()->willReturn('email');

        $mailerService->sendProcessLink(
            Argument::type('string'),
            Argument::type('string'),
            Argument::type('string'),
            Argument::type('string')
        )->willReturn([]);

        $credentialService->delete($request, Argument::type('string'))->shouldBeCalled();

        $this->createCredential($request, true)->shouldBeArray();
    }

    public function it_creates_credential_4(
        Request $request,
        GuardInterface $guard,
        MailerService $mailerService,
        CredentialService $credentialService
    ): void {
        $request->getMethod()->willReturn('POST');
        $guard->setUniqueId()->willReturn('id');
        $mailerService->setProcessId()->willReturn('activation_id');

        $credentialService->create(
            Argument::type(Request::class),
            Argument::type('bool'),
            Argument::type('string'),
            Argument::type('string')
        )->willReturn(null);

        $credentialService->getRef()->willReturn('email');

        $mailerService->sendProcessLink(
            Argument::type('string'),
            Argument::type('string'),
            Argument::type('string'),
            Argument::type('string')
        )->willReturn(null);

        $this->createCredential($request, true)->shouldReturn(null);
    }

    public function it_activates_credential_1(
        CredentialService $credentialService
    ): void {
        $credentialService->getOneById(Argument::type('string'))->willReturn(null);

        $this->activateCredential(Argument::type('string'), Argument::type('string'))->shouldReturn([]);
    }

    public function it_activates_credential_2(
        CredentialService $credentialService,
        CredentialDTO $credential
    ): void {
        $credentialService->getOneById(Argument::type('string'))->willReturn($credential);

        $credential->convert()->willReturn(['is_active' => 1]);

        $this->activateCredential(Argument::type('string'), Argument::type('string'))
            ->shouldReturn(null);
    }

    public function it_activates_credential_3(
        CredentialService $credentialService,
        CredentialDTO $credential
    ): void {
        $credentialService->getOneById(Argument::type('string'))->willReturn($credential);

        $credential->convert()->willReturn(['is_active' => 0, 'created_at' => '2020-01-01 01:01:01']);

        $credentialService->deleteStatic(Argument::type('string'))->shouldBeCalled();

        $this->activateCredential(Argument::type('string'), Argument::type('string'))
            ->shouldReturn([]);
    }

    public function it_activates_credential_4(
        CredentialService $credentialService,
        CredentialDTO $credential,
        GuardInterface $guard
    ): void {
        $credentialService->getOneById(Argument::type('string'))->willReturn($credential);

        $createdAt = new DateTimeImmutable('now');

        $credential->convert()->willReturn([
            'is_active' => 0,
            'created_at' => $createdAt->format('Y-m-d H:i:s'),
            'activation_id' => 'activation_id'
        ]);

        $guard->hash(Argument::type('string'))->willReturn('activation_id_hash');

        $guard->verifyHash(Argument::type('string'), Argument::type('string'))->willReturn(false);

        $this->activateCredential(Argument::type('string'), Argument::type('string'))
            ->shouldReturn([]);
    }

    public function it_activates_credential_5(
        CredentialService $credentialService,
        CredentialDTO $credential,
        GuardInterface $guard
    ): void {
        $credentialService->getOneById(Argument::type('string'))->willReturn($credential);

        $createdAt = new DateTimeImmutable('now');

        $credential->convert()->willReturn([
            'is_active' => 0,
            'created_at' => $createdAt->format('Y-m-d H:i:s'),
            'activation_id' => 'activation_id'
        ]);

        $guard->hash(Argument::type('string'))->willReturn('activation_id_hash');

        $guard->verifyHash(Argument::type('string'), Argument::type('string'))->willReturn(true);

        $credentialService->updateStatic(
            Argument::type('string'),
            Argument::type(DataObjectInterface::class)
        )->willReturn(null);

        $this->activateCredential(Argument::type('string'), Argument::type('string'))
            ->shouldReturn(null);
    }

    public function it_activates_credential_6(
        CredentialService $credentialService,
        CredentialDTO $credential,
        HydratorInterface $hydrator,
        GuardInterface $guard
    ): void {
        $credentialService->getOneById(Argument::type('string'))->willReturn($credential);

        $createdAt = new DateTimeImmutable('now');

        $credential->convert()->willReturn([
            'is_active' => 0,
            'created_at' => $createdAt->format('Y-m-d H:i:s'),
            'activation_id' => 'activation_id'
        ]);

        $guard->hash(Argument::type('string'))->willReturn('activation_id_hash');

        $guard->verifyHash(Argument::type('string'), Argument::type('string'))->willReturn(true);

        $credentialService->updateStatic(
            Argument::type('string'),
            Argument::type(DataObjectInterface::class)
        )->willReturn([]);

        $this->activateCredential(Argument::type('string'), Argument::type('string'))
            ->shouldReturn([]);
    }

    public function it_logs_in_credential_1(
        Request $request,
        CredentialService $credentialService
    ): void {
        $credentialService->checkOneBy($request)->willReturn(null);

        $this->loginCredential($request)->shouldReturn(null);
    }

    public function it_logs_in_credential_2(
        Request $request,
        CredentialService $credentialService
    ): void {
        $credentialService->checkOneBy($request)->willReturn([]);

        $this->loginCredential($request)->shouldBeArray();
    }

    public function it_restores_credential_1(
        Request $request,
        CredentialService $credentialService
    ): void
    {
        $request->getMethod()->willReturn('GET');

        $credentialService->restoreOneBy($request, null)->willReturn([]);

        $this->restoreCredential($request)->shouldBeArray();
    }

    public function it_restores_credential_2(
        Request $request,
        CredentialService $credentialService,
        MailerService $mailerService
    ): void {
        $request->getMethod()->willReturn('POST');

        $mailerService->setProcessId()->willReturn('reset_id');

        $credentialService->restoreOneBy($request, Argument::type('string'))->willReturn([]);

        $this->restoreCredential($request)->shouldBeArray();
    }

    public function it_restores_credential_3(
        Request $request,
        CredentialService $credentialService,
        MailerService $mailerService
    ): void {
        $request->getMethod()->willReturn('POST');

        $mailerService->setProcessId()->willReturn('reset_id');

        $credentialService->restoreOneBy($request, Argument::type('string'))->willReturn(null);

        $credentialService->getId()->willReturn('id');
        $credentialService->getRef()->willReturn('email');

        $mailerService->sendProcessLink(
            Argument::type('string'),
            Argument::type('string'),
            Argument::type('string'),
            Argument::type('string')
        )->willReturn([]);

        $this->restoreCredential($request)->shouldBeArray();
    }

    public function it_restores_credential_4(
        Request $request,
        CredentialService $credentialService,
        MailerService $mailerService
    ): void {
        $request->getMethod()->willReturn('POST');

        $mailerService->setProcessId()->willReturn('reset_id');

        $credentialService->restoreOneBy($request, Argument::type('string'))->willReturn(null);

        $credentialService->getId()->willReturn('id');
        $credentialService->getRef()->willReturn('email');

        $mailerService->sendProcessLink(
            Argument::type('string'),
            Argument::type('string'),
            Argument::type('string'),
            Argument::type('string')
        )->willReturn(null);

        $this->restoreCredential($request)->shouldReturn(null);
    }

    public function it_resets_credential_1(
        CredentialService $credentialService
    ): void {
        $credentialService->getOneById(Argument::type('string'))->willReturn(null);

        $this->resetCredential(Argument::type('string'), Argument::type('string'))->shouldBeArray();
    }

    public function it_resets_credential_2(
        CredentialService $credentialService,
        CredentialDTO $credential
    ): void {
        $credentialService->getOneById(Argument::type('string'))->willReturn($credential);

        $credential->convert()->willReturn(['updated_at' => '2020-01-01 01:01:01']);

        $this->resetCredential(Argument::type('string'), Argument::type('string'))
            ->shouldReturn([]);
    }

    public function it_resets_credential_3(
        CredentialService $credentialService,
        CredentialDTO $credential,
        GuardInterface $guard
    ): void {
        $credentialService->getOneById(Argument::type('string'))->willReturn($credential);

        $createdAt = new DateTimeImmutable('now');

        $credential->convert()->willReturn([
            'updated_at' => $createdAt->format('Y-m-d H:i:s'),
            'reset_id' => 'activation_id'
        ]);

        $guard->hash(Argument::type('string'))->willReturn('reset_id_hash');

        $guard->verifyHash(Argument::type('string'), Argument::type('string'))->willReturn(false);

        $this->resetCredential(Argument::type('string'), Argument::type('string'))
            ->shouldReturn([]);
    }

    public function it_resets_credential_4(
        CredentialService $credentialService,
        CredentialDTO $credential,
        GuardInterface $guard,
        SessionInterface $session
    ): void {
        $credentialService->getOneById(Argument::type('string'))->willReturn($credential);

        $createdAt = new DateTimeImmutable('now');

        $credential->convert()->willReturn([
            'updated_at' => $createdAt->format('Y-m-d H:i:s'),
            'reset_id' => 'activation_id'
        ]);

        $guard->hash(Argument::type('string'))->willReturn('reset_id_hash');

        $guard->verifyHash(Argument::type('string'), Argument::type('string'))->willReturn(true);

        $session->setData(Argument::type('array'), Argument::type('string'))->shouldBeCalled();

        $this->resetCredential(Argument::type('string'), Argument::type('string'))
            ->shouldReturn(null);
    }

    public function it_restate_credential_1(
        SessionInterface $session,
        CredentialService $credentialService,
        Request $request
    ): void {
        $session->getData(Argument::type('string'))
            ->shouldBeCalledTimes(2)
            ->willReturn(['id' => 'row_id', 'hash' => 'row_hash']);

        $credentialService->getOneById(Argument::type('string'))->willReturn(null);

        $this->restateCredential($request)->shouldBeArray();
    }

    public function it_restate_credential_2(
        SessionInterface $session,
        CredentialService $credentialService,
        CredentialDTO $credential,
        GuardInterface $guard,
        Request $request
    ): void {
        $session->getData(Argument::type('string'))
            ->shouldBeCalledTimes(2)
            ->willReturn(['id' => 'row_id', 'hash' => 'row_hash']);

        $credentialService->getOneById(Argument::type('string'))->willReturn($credential);

        $credential->convert()->willReturn(['reset_id' => 'reset_id']);

        $guard->hash(Argument::type('string'))->willReturn('reset_id_hash');

        $guard->verifyHash(Argument::type('string'), Argument::type('string'))->willReturn(false);

        $this->restateCredential($request)->shouldBeArray();
    }

    public function it_restate_credential_3(
        SessionInterface $session,
        CredentialService $credentialService,
        CredentialDTO $credential,
        GuardInterface $guard,
        Request $request
    ): void {
        $session->getData(Argument::type('string'))
            ->shouldBeCalledTimes(2)
            ->willReturn(['id' => 'row_id', 'hash' => 'row_hash']);

        $credentialService->getOneById(Argument::type('string'))->willReturn($credential);

        $credential->convert()->willReturn(['reset_id' => 'reset_id']);

        $guard->hash(Argument::type('string'))->willReturn('reset_id_hash');

        $guard->verifyHash(Argument::type('string'), Argument::type('string'))->willReturn(true);

        $credentialService->update(Argument::type(Request::class), Argument::type('string'))->willReturn([]);

        $this->restateCredential($request)->shouldBeArray();
    }

    public function it_restate_credential_4(
        SessionInterface $session,
        CredentialService $credentialService,
        CredentialDTO $credential,
        GuardInterface $guard,
        Request $request
    ): void {
        $session->getData(Argument::type('string'))
            ->shouldBeCalledTimes(2)
            ->willReturn(['id' => 'row_id', 'hash' => 'row_hash']);

        $credentialService->getOneById(Argument::type('string'))->willReturn($credential);

        $credential->convert()->willReturn(['reset_id' => 'reset_id']);

        $guard->hash(Argument::type('string'))->willReturn('reset_id_hash');

        $guard->verifyHash(Argument::type('string'), Argument::type('string'))->willReturn(true);

        $credentialService->update(Argument::type(Request::class), Argument::type('string'))
            ->willReturn(null);

        $session->unsetData()->shouldBeCalled();

        $this->restateCredential($request)->shouldReturn(null);
    }

    public function it_update_credential_1(
        SessionInterface $session,
        Request $request,
        CredentialService $credentialService
    ): void {
        $session->getData('authentication')->willReturn(['id' => 'id']);

        $credentialService->update($request, Argument::type('string'))->willReturn([]);

        $this->updateCredential($request)->shouldBeArray();
    }

    public function it_update_credential_2(
        SessionInterface $session,
        Request $request,
        CredentialService $credentialService,
        DataObjectInterface $hydrator
    ): void {
        $session->getData('authentication')->willReturn(['id' => 'id.dudu']);

        $credentialService->update($request, Argument::type('string'))->willReturn(null);

        $credentialService->getOneById(Argument::type('string'))->willReturn($hydrator);

        $hydrator->convert()->willReturn([]);

        $session->setData(Argument::type('array'), Argument::type('string'))->shouldBeCalled();

        $session->setFlash(Argument::type('array'), Argument::type('string'))->shouldBeCalled();

        $this->updateCredential($request)->shouldReturn(null);
    }

    public function it_deletes_credential_1(
        Request $request,
        SessionInterface $session
    ): void {
        $request->getMethod()->willReturn('GET');

        $session->setFlash(['credentials_unsubscribe_warning'], 'warning')->shouldBeCalled();

        $this->deleteCredential($request, null)->shouldBeArray();
    }

    public function it_deletes_credential_2(
        Request $request,
        SessionInterface $session,
        CredentialService $credentialService
    ): void {
        $request->getMethod()->willReturn('POST');

        $session->getData('authentication')->willReturn(['id' => 'id']);
        $session->unsetData()->shouldBeCalled();

        $credentialService->deleteStatic(Argument::type('string'))->shouldBeCalled();

        $this->deleteCredential($request, null)->shouldReturn(null);
    }

    public function it_logouts_credential_1(Request $request, SessionInterface $session): void
    {
        $request->getMethod()->willReturn('GET');

        $session->setFlash(['credentials_logout_warning'], 'warning');

        $this->logoutCredential($request)->shouldBeArray();
    }

    public function it_logouts_credential_2(Request $request, SessionInterface $session): void
    {
        $request->getMethod()->willReturn('POST');

        $session->unsetData()->shouldBeCalled();

        $this->logoutCredential($request)->shouldReturn(null);
    }
}
