<?php

namespace spec\Paneric\Authentication\Service;

use Paneric\Authentication\Service\MailerService;
use Paneric\Interfaces\Guard\GuardInterface;
use PHPMailer\PHPMailer\PHPMailer;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class MailerServiceSpec extends ObjectBehavior
{
    public function let(
        GuardInterface $guard,
        PHPMailer $mailer
    ): void {
        $config = [
            'random_string_length' => 128,

            'activation_url' => 'http://localhost8080/activation/',
            'activation_time_delay' => 24,

            'reset_url' => '',
            'reset_time_delay' => 24,

            'mail_content' => [
                'activation_email_subject' => [
                    'en'   => 'Activation email subject.',
                ],
                'activation_email_message' => [
                    'en'   => 'Activation email message.',
                ],
            ],
        ];

        $this->beConstructedWith($guard, $mailer, $config, 'en');
    }

    function it_is_initializable(): void
    {
        $this->shouldHaveType(MailerService::class);
    }

    public function it_sets_process_id(GuardInterface $guard): void
    {
        $guard->generateRandomString(Argument::type('integer'))->willReturn('activation_id');

        $this->setProcessId()->shouldReturn('activation_id');
    }

    public function it_sends_activation_link(PHPMailer $mailer, GuardInterface $guard): void
    {
        $mailer->addAddress(Argument::type('string'))->willReturn('email_address');
        $guard->hash(Argument::type('string'))->willReturn('activation_id_hash');

        $mailer->send()->willReturn(true);

        $this->sendProcessLink('activation','activation_id', 'user_id', 'email')
            ->shouldReturn(null);
    }
}
