<?php

namespace spec\Paneric\Authentication\Service;

use Paneric\Authentication\DBAL\CredentialDTO;
use Paneric\Authentication\DBAL\CredentialRepositoryInterface;
use Paneric\Authentication\Service\CredentialService;
use Paneric\Authentication\Service\SerializerService;
use Paneric\Interfaces\DataObject\DataObjectInterface;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\Interfaces\Session\SessionInterface;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Http\Message\ServerRequestInterface as Request;

class CredentialServiceSpec extends ObjectBehavior
{
    public function let(
        CredentialRepositoryInterface $credentialRepository,
        SerializerService $serializerService,
        GuardInterface $guard,
        SessionInterface $session
    ): void {
        $config = [
            'role_id' => 3,
        ];

        $this->beConstructedWith($credentialRepository, $session, $serializerService, $guard, $config);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(CredentialService::class);
    }

    public function it_gets_all_paginated(
        SessionInterface $session,
        CredentialRepositoryInterface $credentialRepository,
        SerializerService $serializerService
    ): void {
        $session->setFlash(
            Argument::type('array'),
            Argument::type('string')
        )->shouldBeCalled();

        $session->getData(Argument::type('string'))
            ->shouldBeCalled()
            ->willReturn(['limit' => 1, 'offset' => 1]);

        $credentialRepository->findBy(
            Argument::type('array'),
            Argument::type('array'),
            Argument::type('int'),
            Argument::type('int')
        )->shouldBeCalled()
            ->willReturn([]);

        $serializerService->jsonSerializeObjects([])
            ->shouldBeCalled()->willReturn([]);

        $this->getAllPaginated()->shouldBeArray();
    }

    public function it_gets_one_by_id_1(
        CredentialRepositoryInterface $credentialRepository,
        DataObjectInterface $object
    ): void {

        $credentialRepository->findOneBy(Argument::type('array'))
            ->shouldBeCalled()
            ->willReturn($object);

        $this->getOneById(Argument::type('string'))->shouldReturn($object);
    }

    public function it_gets_one_by_id_2(
        CredentialRepositoryInterface $credentialRepository
    ): void {

        $credentialRepository->findOneBy(Argument::type('array'))
            ->shouldBeCalled()
            ->willReturn(null);

        $this->getOneById(Argument::type('string'))->shouldReturn(null);
    }

    public function it_checks_one_by_1(Request $request): void
    {
        $request->getParsedBody()->willReturn([
            'ref' => 'email',
            'password_hash' => 'password_hash'
        ]);

        $request->getMethod()->willReturn('GET');

        $this->checkOneBy($request)->shouldBeArray();
    }

    public function it_checks_one_by_2(
        Request $request,
        CredentialRepositoryInterface $credentialRepository,
        SessionInterface $session
    ): void {
        $attributes = ['ref' => 'email', 'password_hash' => 'password_hash'];

        $request->getParsedBody()->willReturn($attributes);

        $request->getMethod()->willReturn('POST');

        $request->getAttribute('validation')->willReturn([CredentialDTO::class => []]);

        $credentialRepository->findOneBy(Argument::type('array'))->willReturn(null);

        $session->setFlash(['db_credentials_login_error'], 'error')->shouldBeCalled();

        $this->checkOneBy($request)->shouldBeArray();
    }

    public function it_checks_one_by_3(
        Request $request,
        GuardInterface $guard,
        CredentialRepositoryInterface $credentialRepository,
        SessionInterface $session,
        CredentialDTO $credential
    ): void {
        $attributes = ['ref' => 'email', 'password_hash' => 'password_hash'];

        $request->getParsedBody()->willReturn($attributes);

        $request->getMethod()->willReturn('POST');

        $request->getAttribute('validation')->willReturn([CredentialDTO::class => []]);

        $credentialRepository->findOneBy(Argument::type('array'))->willReturn($credential);

        $credential->getIsActive()->willReturn(false);

        $session->setFlash(['db_credentials_activation_error'], 'error')->shouldBeCalled();

        $this->checkOneBy($request)->shouldBeArray();
    }

    public function it_checks_one_by_4(
        Request $request,
        GuardInterface $guard,
        CredentialRepositoryInterface $credentialRepository,
        SessionInterface $session,
        CredentialDTO $credential
    ): void {
        $attributes = ['ref' => 'email', 'password_hash' => 'password_hash'];

        $request->getParsedBody()->willReturn($attributes);

        $request->getMethod()->willReturn('POST');

        $request->getAttribute('validation')->willReturn([CredentialDTO::class => []]);

        $credentialRepository->findOneBy(Argument::type('array'))->willReturn($credential);

        $credential->getIsActive()->willReturn(true);

        $credential->convert()->willReturn($attributes);

        $guard->verifyPassword(Argument::type('string'), Argument::type('string'))->willReturn(false);

        $session->setFlash(['db_credentials_login_error'], 'error')->shouldBeCalled();

        $this->checkOneBy($request)->shouldBeArray();
    }

    public function it_checks_one_by_5(
        Request $request,
        GuardInterface $guard,
        CredentialRepositoryInterface $credentialRepository,
        SessionInterface $session,
        CredentialDTO $credential
    ): void {
        $attributes = ['ref' => 'email', 'password_hash' => 'password_hash'];

        $request->getParsedBody()->willReturn($attributes);

        $request->getMethod()->willReturn('POST');

        $request->getAttribute('validation')->willReturn([CredentialDTO::class => []]);

        $credentialRepository->findOneBy(Argument::type('array'))->willReturn($credential);

        $credential->getIsActive()->willReturn(true);

        $credential->convert()->willReturn($attributes);

        $guard->verifyPassword(Argument::type('string'), Argument::type('string'))->willReturn(true);

        $session->setData(Argument::type('array'), 'authentication')->shouldBeCalled();

        $this->checkOneBy($request)->shouldReturn(null);
    }

    public function it_restores_one_by_1(Request $request): void
    {
        $request->getMethod()->willReturn('GET');

        $this->restoreOneBy($request)->shouldBeArray();
    }

    public function it_restores_one_by_2(Request $request): void
    {
        $request->getMethod()->willReturn('POST');

        $request->getAttribute('validation')->willReturn([CredentialDTO::class => 'not_empty']);

        $request->getParsedBody()->willReturn([]);

        $this->restoreOneBy($request)->shouldBeArray();
    }

    public function it_restores_one_by_3(
        Request $request,
        CredentialRepositoryInterface $credentialRepository
    ): void{
        $request->getMethod()->willReturn('POST');

        $request->getAttribute('validation')->willReturn([CredentialDTO::class => []]);

        $request->getParsedBody()->willReturn(['ref' => 'email']);

        $credentialRepository->findOneBy(Argument::type('array'))->willReturn(null);

        $this->restoreOneBy($request)->shouldBeArray();
    }

    public function it_restores_one_by_4(
        Request $request,
        CredentialRepositoryInterface $credentialRepository,
        DataObjectInterface $object,
        CredentialDTO $credential
    ): void {
        $request->getMethod()->willReturn('POST');

        $request->getAttribute('validation')->willReturn([CredentialDTO::class => []]);

        $request->getParsedBody()->willReturn(['ref' => 'email']);

        $credentialRepository->findOneBy(Argument::type('array'))->willReturn($object);

        $object->convert()->willReturn(['id' => 'id', 'ref' => 'email']);

        $object->hydrate(Argument::type('array'))->willReturn($credential);

        $credentialRepository->update(Argument::type('array'), Argument::type(DataObjectInterface::class))
            ->willReturn(1);

        $this->restoreOneBy($request, Argument::type('string'))->shouldReturn(null);
    }

    public function it_creates_1(Request $request): void
    {
        $request->getMethod()->willReturn('GET');

        $this->create($request, true)->shouldBeArray();
    }

    public function it_creates_2(Request $request): void
    {
        $request->getMethod()->willReturn('POST');

        $request->getParsedBody()->willReturn(['password_hash' => 'password', 'ref' => 'email']);

        $request->getAttribute('captcha')->willReturn(['success' => false]);

        $this->create($request, true)->shouldBeArray();
    }

    public function it_creates_3(Request $request): void
    {
        $request->getMethod()->willReturn('POST');

        $request->getParsedBody()->willReturn(['password_hash' => 'password', 'ref' => 'email']);

        $request->getAttribute('captcha')->willReturn(['success' => true]);

        $request->getAttribute('validation')->willReturn([CredentialDTO::class => ['not_empty']]);

        $this->create($request, true)->shouldBeArray();
    }

    public function it_creates_4(
        Request $request,
        CredentialRepositoryInterface $credentialRepository,
        DataObjectInterface $object
    ): void {
        $request->getMethod()->willReturn('POST');

        $request->getParsedBody()->willReturn(['password_hash' => 'password', 'ref' => 'email']);

        $request->getAttribute('captcha')->willReturn(['success' => true]);

        $request->getAttribute('validation')->willReturn([CredentialDTO::class => []]);

        $credentialRepository->findOneBy(Argument::type('array'))->willReturn($object);

        $this->create($request, true)->shouldBeArray();
    }

    public function it_creates_5(
        Request $request,
        GuardInterface $guard,
        CredentialRepositoryInterface $credentialRepository
    ): void {
        $request->getMethod()->willReturn('POST');

        $request->getParsedBody()->willReturn(['password_hash' => 'password', 'ref' => 'email']);

        $request->getAttribute('captcha')->willReturn(['success' => true]);

        $request->getAttribute('validation')->willReturn([CredentialDTO::class => []]);

        $credentialRepository->findOneBy(Argument::type('array'))->willReturn(null);

        $guard->hashPassword(Argument::type('string'))->willReturn('password_hash');

        $credentialRepository->create(Argument::type(DataObjectInterface::class))->willReturn('0');

        $this->create($request, true)->shouldReturn(null);
    }

    public function it_updates_1(Request $request): void
    {
        $request->getMethod()->willReturn('GET');

        $this->update($request, 'id')->shouldBeArray();
    }

    public function it_updates_2(Request $request): void
    {
        $request->getMethod()->willReturn('POST');

        $request->getAttribute('validation')->willReturn([CredentialDTO::class => 'message']);

        $request->getParsedBody()->willReturn([]);

        $this->update($request, 'id')->shouldBeArray();
    }

    public function it_updates_3(
        Request $request,
        CredentialRepositoryInterface $credentialRepository,
        SessionInterface $session,
        DataObjectInterface $object
    ): void {
        $request->getMethod()->willReturn('POST');

        $request->getAttribute('validation')->willReturn([CredentialDTO::class => []]);

        $request->getParsedBody()->willReturn(['ref' => 'email']);

        $session->getData(Argument::type('string'))->shouldNotBeCalled();

        $credentialRepository->findByEnhanced(
            Argument::type('array'),
            Argument::type('string'),
            Argument::type('string')
        )->willReturn([1]);

        $session->setFlash(Argument::type('array'), Argument::type('string'))->shouldBeCalled();

        $credentialRepository->findOneBy(Argument::type('array'))->willReturn($object);

        $this->update($request, 'id')->shouldBeArray();
    }

    public function it_updates_4(
        Request $request,
        CredentialRepositoryInterface $credentialRepository,
        SessionInterface $session,
        GuardInterface $guard
    ): void {
        $request->getMethod()->willReturn('POST');

        $request->getAttribute('validation')->willReturn([CredentialDTO::class => []]);

        $request->getParsedBody()->willReturn(['password_hash' => 'hash']);

        $session->getData(Argument::type('string'))->shouldNotBeCalled();

        $credentialRepository->findByEnhanced(
            Argument::type('array'),
            Argument::type('string'),
            Argument::type('string')
        )->shouldNotBeCalled();

        $session->setFlash(Argument::type('array'), Argument::type('string'))->shouldNotBeCalled();

        $credentialRepository->findOneBy(Argument::type('array'))->shouldNotBeCalled();

        $guard->hashPassword(Argument::type('string'))->willReturn('hash');

        $credentialRepository->update(Argument::type('array'), Argument::type(DataObjectInterface::class))
            ->shouldBeCalled()
            ->willReturn(1);

        $this->update($request, 'id')->shouldReturn(null);
    }

    public function it_updates_5(
        Request $request,
        CredentialRepositoryInterface $credentialRepository,
        SessionInterface $session,
        GuardInterface $guard
    ): void {
        $request->getMethod()->willReturn('POST');

        $request->getAttribute('validation')->willReturn([CredentialDTO::class => []]);

        $request->getParsedBody()->willReturn(['ref' => 'email', 'password_hash' => 'hash']);

        $session->getData(Argument::type('string'))->shouldNotBeCalled();

        $credentialRepository->findByEnhanced(
            Argument::type('array'),
            Argument::type('string'),
            Argument::type('string')
        )->willReturn([]);

        $session->setFlash(Argument::type('array'), Argument::type('string'))->shouldNotBeCalled();

        $credentialRepository->findOneBy(Argument::type('array'))->shouldNotBeCalled();

        $guard->hashPassword(Argument::type('string'))->willReturn('hash');

        $credentialRepository->update(Argument::type('array'), Argument::type(DataObjectInterface::class))
            ->shouldBeCalled()
            ->willReturn(1);

        $this->update($request, 'id')->shouldReturn(null);
    }

    public function it_deletes_1(
        Request $request,
        SessionInterface $session
    ): void {
        $request->getMethod()->shouldBeCalled()->willReturn('GET');

        $session->setFlash(Argument::type('array'), Argument::type('string'))->shouldBeCalledTimes(2);

        $this->delete($request, 'id')->shouldReturn([]);
    }

    public function it_deletes_2(
        Request $request,
        CredentialRepositoryInterface $credentialRepository,
        SessionInterface $session
    ): void {
        $request->getMethod()->shouldBeCalled()->willReturn('POST');

        $credentialRepository->delete(Argument::type('array'))->shouldBeCalled()->willReturn(0);

        $session->setFlash(Argument::type('array'), Argument::type('string'))->shouldBeCalled();

        $this->delete($request, 'id')->shouldReturn([]);
    }

    public function it_deletes_3(
        Request $request,
        CredentialRepositoryInterface $credentialRepository,
        SessionInterface $session
    ): void {
        $request->getMethod()->shouldBeCalled()->willReturn('POST');

        $credentialRepository->delete(Argument::type('array'))->shouldBeCalled()->willReturn(1);

        $this->delete($request, 'id')->shouldReturn(null);
    }

    public function it_updates_static_1(
        CredentialRepositoryInterface $credentialRepository,
        DataObjectInterface $object
    ): void {
        $credentialRepository->update(Argument::type('array'), Argument::type(DataObjectInterface::class))
            ->willReturn(0);

        $this->updateStatic(Argument::type('string'), $object)
            ->shouldReturn([]);
    }

    public function it_updates_static_2(
        CredentialRepositoryInterface $credentialRepository,
        DataObjectInterface $object
    ): void {
        $credentialRepository->update(Argument::type('array'), Argument::type(DataObjectInterface::class))
            ->willReturn(1);

        $this->updateStatic(Argument::type('string'), $object)
            ->shouldReturn(null);
    }
}
