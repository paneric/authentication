<?php

namespace spec\Paneric\Authentication;

use Paneric\Authentication\RecaptchaV3Middleware;
use Paneric\Interfaces\Session\SessionInterface;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

define('ROOT_FOLDER', dirname(__DIR__) . '/');

class RecaptchaV3MiddlewareSpec extends ObjectBehavior
{
    public function let(SessionInterface $session): void
    {
        $config = [
            'site_key' => '6Ld3BtsUAAAAAAxzelj69p2_UdfCqUPMmC8YLSSE',
            'url_api_script' => '<script src="https://www.google.com/recaptcha/api.js?render=%s"></script>',
            'url_site_verify' => 'https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s',
            'non_ascii_key' => '6LceLdsUAAAAACGy8dkfISYhq5IimN-LHhMkd91L',
            'file_path' => ROOT_FOLDER . 'temp2.txt',
            'score' => 0.5,
        ];

        $this->beConstructedWith($session, $config);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(RecaptchaV3Middleware::class);
    }

    public function it_processes_1(
        SessionInterface $session,
        ServerRequestInterface $request,
        RequestHandlerInterface $handler,
        ResponseInterface $response
    ): void {
        $session->setFlash(Argument::type('array'), Argument::type('string'))->shouldBeCalled();

        $request->getMethod()->willReturn('GET');

        $handler->handle($request)->willReturn($response);

        $this->process($request, $handler)->shouldReturn($response);
    }

    public function it_processes_2(
        SessionInterface $session,
        ServerRequestInterface $request,
        RequestHandlerInterface $handler,
        ResponseInterface $response
    ): void {
        $session->setFlash(Argument::type('array'), Argument::type('string'))->shouldBeCalled();

        $request->getMethod()->willReturn('POST');

        $request->getParsedBody()->willReturn(['g-recaptcha-response' => 'recaptcha_response']);

        $session->setFlash(Argument::type('array'), Argument::type('string'))->shouldBeCalled();

        $request->withAttribute(Argument::type('string'), Argument::type('array'))->willReturn($request);

        $handler->handle($request)->willReturn($response);

        $this->process($request, $handler)->shouldReturn($response);
    }
}
