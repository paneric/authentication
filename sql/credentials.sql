SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE TABLE `credentials` (
                               `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                               `role_id` int(11) NOT NULL,
                               `ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                               `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                               `terms` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                               `is_active` tinyint(1) NOT NULL DEFAULT '0',
                               `activation_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                               `reset_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                               `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)',
                               `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `credentials`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `ref_idx` (`ref`),
    ADD UNIQUE KEY `password_hash_idx` (`password_hash`),
    ADD KEY `is_active_idx` (`is_active`),
    ADD KEY `activation_id_idx` (`activation_id`),
    ADD KEY `reset_id_idx` (`reset_id`),
    ADD KEY `created_at_idx` (`created_at`),
    ADD KEY `updated_at_idx` (`updated_at`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;