<?php

return [
    'file_path' => ROOT_FOLDER . 'temp.txt',
    'algo_password' => PASSWORD_BCRYPT,
    'options_algo_password' => [
        'cost' => 10,
    ],
    'algo_hash' => 'sha512',
    'unique_id_prefix' => '',
    'unique_id_more_entropy' => true,
];
