<?php

return [
    /* common */

    'main' => 'Strona główna',
    'authentication' => 'Uwierzytelnienie',
    'start' => 'Start',
    'credential' => 'Loginy',
    'credentials' => 'Loginy',

    'email_address' => 'Adres e-mail',
    'email_address_help' => 'Twój e-mail nigdy nie zostanie przekazany osobom trzecim',
    'password' => 'Hasło',
    'password_help' => 'Twoje hasło musi zawierać min 8 znaków w tym przynajmniej jedna dużą literę, jedna liczbę oraz jeden charakter specjalny',

    'role_id' => 'ID roli',
    'role_id_help' => 'ID roli - liczba naturalna',

    'cancel' => 'Anuluj',
    'add' => 'Dodaj',
    'confirm' => 'Potwierdź',
    'modify' => 'Modyfikuj',
    'update' => 'Uaktualnij',
    'delete' => 'Usuń',
    'restore' => 'Odzyskaj',
    'choose' => 'Wybierz...',
    'back' => 'Wróć',

    /* 'authe.credentials.show' */

    'content_credentials_show_title' => 'Loginy',
    
    /* authe.credential.subscribe */

    'form_subscribe_title' => 'Załóż konto',
    'repeat_password' => 'Powtórz hasło',
    'i_accept' => 'Potwierdzam',
    'i_accept_terms' => 'Niniejszym potwierdzasz swoja akceptacje warunków ogólnych oraz regulaminu serwisu.',
    'subscribe' => 'Załóż konto',

    'credentials_create_success' =>'Twoje konto zostało utworzone.',
    'credentials_activate_info' => 'W celu aktywacji konta sprawdź swój e-mail i kliknij link aktywacyjny.',

    'db_credentials_subscribe_error' => 'Wprowadzony adres e-mail jest już użyty..',

    /* authe.credential.subscribe - recaptcha*/

    'invalid_security_score' => 'Security score not sufficient.',
    'missing-input-secret' => 'The secret parameter is missing.',
    'invalid-input-secret' => 'The secret parameter is invalid or malformed.',
    'missing-input-response' => 'The response parameter is missing.',
    'invalid-input-response' =>	'The response parameter is invalid or malformed.',
    'bad-request' => 'The request is invalid or malformed.',
    'timeout-or-duplicate' => 'The response is no longer valid: either is too old or has been used previously.',

    /* authe.credential.activate */

    'credentials_activate_success' => 'Twoje konto zostało aktywowane.',
    'credentials_activate_error' => 'Aktywacja Twojego konta nie powiodła się.',
    'credentials_create_info' => 'Prawdopodobnie czas na aktywacje Twojego konta został przekroczony. Zarejestrj się wymagana.',

    /* authe.credential.login */

    'form_login_title' => 'Zaloguj się',
    'log_in' => 'Logowanie',

    'form_authenticate_title' => 'Uwierzytelnienia',
    'authenticate' => 'Uwierzytelnij',

    'db_credentials_activation_error' => 'Twoje konto nie jest aktywowane.',
    'db_credentials_login_error' => 'Wprowadzony e-mail lub hasło jest niepoprawne.',

    /* authe.credential.logout */

    'form_logout_title' => 'Wyloguj się',

    'credentials_logout_warning' => 'Potwierdź zamiar wylogowania.',

    /* authe.credential.unsubscribe */

    'form_unsubscribe_title' => 'Usuń konto',

    'credentials_unsubscribe_warning' => 'Potwierdź zamiar usunięcia konta.',

    /* authe.credential.restore */

    'form_restore_title' => 'Odzyskaj dostęp',

    'credentials_restore_success' => 'Identyfikacja Twojego konta zakończyła się pomyślnie.',
    'credentials_restore_info' => 'Proszę sprawdź swój e-mail. W celu resetu hasła kliknij link.',

    'db_credentials_restore_error' => 'Wprowadzony adres e-mail jest niepoprawny.',

    /* authe.credential.reset */

    'credentials_reset_error' => 'Reset Twojego hasła nie powiódł się.',
    'credentials_reset_info' => 'Prawdopodobnie dopuszczalny czas na reset Twojego hasła został przekroczony. Sprobuj ponownie.',

    /* authe.credential.restate */

    'form_restate_title' => 'Wprowadź nowe hasło',
    'restate' => 'Wprowadź',

    /* authe.credential.add */

    'form_add_title' => 'Dodaj loginy',

    /* authe.credential.update */

    'form_update_title' => 'Modyfikuj loginy',
    'new_email' => 'Nowy e-mail',
    'new_password' => 'Nowe hasła',
    'repeat_new_password' => 'Powtórz nowe hasło',

    'credentials_update_success' => 'Modyfikacja danych logowania zakończyła się pomyślnie.',

    /* authe.credential.delete */

    'form_delete_title' => 'Usuń loginy',
    'credentials_delete_warning' => 'Potwierdź zamiar usunięcia danych logowania.',
];
