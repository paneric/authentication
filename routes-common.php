<?php

declare(strict_types=1);

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Paneric\Authentication\AuthenticationController;
use Paneric\OAUTHServer\Controller\CredentialController;
use Paneric\Middleware\CSRFMiddleware;
use Paneric\Validation\ValidationMiddleware;

$app->get('/authe/{id}/{hash}/activate', function(Request $request, Response $response, array $args) {
    return $this->get(AuthenticationController::class)->activateCredential($response, $args['id'], $args['hash']);
})->setName('authe.credential.activate');


$app->map(['GET', 'POST'], '/authe/restore', function(Request $request, Response $response) {
    return $this->get(AuthenticationController::class)->restoreCredential($request, $response);
})->setName('authe.credential.restore')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->get('/authe/{id}/{hash}/reset', function(Request $request, Response $response, array $args) {
    return $this->get(AuthenticationController::class)->resetCredential($response, $args['id'], $args['hash']);
})->setName('authe.credential.reset');

$app->map(['GET', 'POST'], '/authe/restate', function(Request $request, Response $response) {
    return $this->get(AuthenticationController::class)->restateCredential($request, $response);
})->setName('authe.credential.restate')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/authe/update', function(Request $request, Response $response) {
    return $this->get(AuthenticationController::class)->updateCredential($request, $response);
})->setName('authe.credential.update')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));
