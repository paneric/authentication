<?php

declare(strict_types=1);

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Paneric\Authentication\AuthenticationController;
use Paneric\OAUTHServer\Controller\CredentialController;
use Paneric\Authentication\RecaptchaV3Middleware as RecaptchaMiddleware;
use Paneric\Middleware\CSRFMiddleware;
use Paneric\Validation\ValidationMiddleware;

// route name needed by all:
$app->map(['GET', 'POST'], '/authe/subscribe', function(Request $request, Response $response) {
    return $this->get(AuthenticationController::class)->subscribeCredential($request, $response);
})->setName('authe.credential.subscribe')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class))
    ->addMiddleware($container->get(RecaptchaMiddleware::class));
// route name needed by all:
$app->map(['GET', 'POST'], '/authe/login', function(Request $request, Response $response) {
    return $this->get(AuthenticationController::class)->logInCredential($request, $response);
})->setName('authe.credential.login')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

// route name needed by all:
$app->map(['GET', 'POST'],'/authe/unsubscribe', function(Request $request, Response $response) {
    return $this->get(AuthenticationController::class)->unsubscribeCredential($request, $response);
})->setName('authe.credential.unsubscribe')
    ->addMiddleware($container->get(CSRFMiddleware::class));
// route name needed by all:
$app->map(['GET', 'POST'],'/authe/logout', function(Request $request, Response $response) {
    return $this->get(AuthenticationController::class)->logoutCredential($request, $response);
})->setName('authe.credential.logout')
    ->addMiddleware($container->get(CSRFMiddleware::class));

// route name needed by OAUTHServer:
$app->map(['GET', 'POST'], '/authe/authenticate', function(
    Request $request,
    Response $response
) {
    return $this->get(AuthenticationController::class)->authenticate($request, $response);
})->setName('authe.credential.authenticate')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));