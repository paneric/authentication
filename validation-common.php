<?php

declare(strict_types=1);

use Paneric\Authentication\DBAL\CredentialDTO;

return [
    'validation' => [
        'authe.credential.subscribe' => [
            'methods' => ['POST'],
            CredentialDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                        'is_valid_email' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'password_hash' => [
                        'required' => [],
                        'has_min_length' => [8],
                        'is_valid_password' => [],
                    ],
                    'password_hash_repeat' => [
                        'is_same_as' => ['password_hash'],
                    ],
                    'terms' => [
                        'required' => [],
                        'is_one_of' => ['OK'],
                    ],
                ],
            ],
        ],
        'authe.credential.login' => [
            'methods' => ['POST'],
            CredentialDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                        'is_valid_email' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'password_hash' => [
                        'required' => [],
                        'has_min_length' => [8],
                        'is_valid_password' => [],
                    ],
                ],
            ],
        ],
        'authe.credential.restore' => [
            'methods' => ['POST'],
            CredentialDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                        'is_valid_email' => [],
                    ],
                ],
            ],
        ],
        'authe.credential.restate' => [
            'methods' => ['POST'],
            CredentialDTO::class => [
                'rules' => [
                    'password_hash' => [
                        'required' => [],
                        'has_min_length' => [8],
                        'is_valid_password' => [],
                    ],
                    'password_hash_repeat' => [
                        'is_same_as' => ['password_hash'],
                    ],
                ],
            ],
        ],
        'authe.credential.update' => [
            'methods' => ['POST'],
            CredentialDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                        'is_valid_email' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'password_hash' => [
                        'required' => [],
                        'has_min_length' => [8],
                        'is_valid_password' => [],
                    ],
                    'password_hash_repeat' => [
                        'is_same_as' => ['password_hash'],
                    ],
                ],
            ],
        ],
        'authe.credential.login_oauth' => [
            'methods' => ['POST'],
            CredentialDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                        'is_valid_email' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'password_hash' => [
                        'required' => [],
                        'has_min_length' => [8],
                        'is_valid_password' => [],
                    ],
                ],
            ],
        ],

        'authe.credential.authenticate' => [
            'methods' => ['POST'],
            CredentialDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                        'is_valid_email' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'password_hash' => [
                        'required' => [],
                        'has_min_length' => [8],
                        'is_valid_password' => [],
                    ],
                ],
            ],
        ],
    ],
];
