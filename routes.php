<?php

declare(strict_types=1);

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Paneric\Authentication\AuthenticationController;
use Paneric\Middleware\CSRFMiddleware;
use Paneric\Validation\ValidationMiddleware;
use Paneric\Middleware\PaginationMiddleware;

//$app->get('/authe/credentials/show[/{page}]', function (Request $request, Response $response) {
//    return $this->get(AuthenticationController::class)->showCredentials($response);
//})->setName('authe.credentials.show')
//    ->addMiddleware($container->get(PaginationMiddleware::class));

$app->map(['GET', 'POST'], '/authe/credential/add', function (Request $request, Response $response) {
    return $this->get(AuthenticationController::class)->addCredential($request, $response);
})->setName('authe.credential.add')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/authe/credential/{id}/edit', function (Request $request, Response $response, array $args) {
    return $this->get(AuthenticationController::class)->editCredential($request, $response, $args['id']);
})->setName('authe.credential.edit')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/authe/credential/{id}/delete', function (Request $request, Response $response, array $args) {
    return $this->get(AuthenticationController::class)->deleteCredential($request, $response, $args['id']);
})->setName('authe.credential.delete')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

