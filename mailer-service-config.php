<?php

return [
    'random_string_length' => 128,

    'activation_url' => 'http://127.0.0.1:8080/authe',
    'activation_time_delay' => 24,

    'reset_url' => 'http://127.0.0.1:8080/authe',
    'reset_time_delay' => 24,

    'mail_content' => [
        'activation_email_subject' => [
            'en' => 'Account activation automatic message.'
        ],
        'activation_email_message' => [
            'en' => 'Please click the following link within %sh to activate your account: %s/%s/%s/activate .'
        ],
        'reset_email_subject' => [
            'en' => 'Password reset automatic message.'
        ],
        'reset_email_message' => [
            'en' => 'Please click the following link within %sh to reset your account password: %s/%s/%s/reset .'
        ],
    ],
];
