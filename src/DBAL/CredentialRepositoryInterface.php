<?php

declare(strict_types=1);

namespace Paneric\Authentication\DBAL;

use Paneric\DBAL\RepositoryInterface;

interface CredentialRepositoryInterface extends RepositoryInterface {}