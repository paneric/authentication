<?php

declare(strict_types=1);

namespace Paneric\Authentication\DBAL;

use Paneric\DBAL\Manager;
use Paneric\DBAL\Repository;

class CredentialRepository extends Repository implements CredentialRepositoryInterface
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->table = 'credentials';
        $this->daoClass = CredentialDAO::class;
    }
}
