<?php

declare(strict_types=1);

namespace Paneric\Authentication\DBAL;

use Paneric\DataObject\DAO;
use Paneric\DataValues\DateTimeValue;

class CredentialDAO extends DAO
{
    protected $id;
    protected $roleId;
    protected $ref;
    protected $passwordHash;
    protected $passwordHashRepeat;
    protected $terms;
    protected $isActive;
    protected $activationId;
    protected $resetId;

    public function __construct()
    {
        $this->prefix = 'crd_';

        $this->setMaps();
    }

    public function getId(): ?Int
    {
        return $this->id;
    }
    public function getRoleId(): ?Int
    {
        return $this->roleId;
    }
    public function getRef(): ?String
    {
        return $this->ref;
    }
    public function getPasswordHash(): ?String
    {
        return $this->passwordHash;
    }
    public function getPasswordHashRepeat(): ?String
    {
        return $this->passwordHashRepeat;
    }
    public function getTerms(): ?Int
    {
        return $this->terms;
    }
    public function getIsActive(): ?Int
    {
        return $this->isActive;
    }
    public function getActivationId(): ?String
    {
        return $this->activationId;
    }
    public function getResetId(): ?String
    {
        return $this->resetId;
    }
    public function getCreatedAt(): string
    {
        $createdAtValue = new DateTimeValue($this->createdAt);

        return $createdAtValue->format();
    }
    public function getUpdatedAt(): string
    {
        $updatedAtValue = new DateTimeValue($this->updatedAt);

        return $updatedAtValue->format();
    }

    /**
     * @var int|string
     */
    public function setId($id): void
    {
        $this->id = $id;
    }
    /**
     * @var int|string
     */
    public function setRoleId($roleId): void
    {
        $this->roleId = is_array($roleId) ?
            $roleId :
            (int) $roleId;
    }
    public function setRef(String $ref): void
    {
        $this->ref = $ref;
    }
    public function setPasswordHash(String $passwordHash): void
    {
        $this->passwordHash = $passwordHash;
    }
    public function setPasswordHashRepeat(String $passwordHashRepeat): void
    {
        $this->passwordHashRepeat = $passwordHashRepeat;
    }
    /**
     * @var int|string
     */
    public function setTerms($terms): void
    {
        $this->terms = $terms;
    }
    /**
     * @var int|string
     */
    public function setIsActive($isActive): void
    {
        $this->isActive = is_array($isActive) ?
            $isActive :
            (int) $isActive;
    }
    public function setActivationId(String $activationId): void
    {
        $this->activationId = $activationId;
    }
    public function setResetId(String $resetId): void
    {
        $this->resetId = $resetId;
    }
}
