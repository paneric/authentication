<?php

declare(strict_types=1);

namespace Paneric\Authentication;

use Paneric\Authentication\Service\AuthenticationService;
use Paneric\CSRTriad\Controller\AppController;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class AuthenticationController extends AppController
{
    private $authenticationService;

    public function __construct(AuthenticationService $authenticationService, Twig $twig)
    {
        parent::__construct($twig);

        $this->authenticationService = $authenticationService;
    }

    public function showCredentials(Response $response): Response // (GET) /xxx
    {
        return $this->render(
            $response,
            '@module/show.html.twig',
            $this->authenticationService->getAllCredentials()
        );
    }

    public function addCredential(Request $request, Response $response): Response
    {
        $result = $this->authenticationService->createCredential($request, false);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/authe/credentials/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/add.html.twig',
            $result
        );
    }

    public function editCredential(Request $request, Response $response, string $id): Response // (POST) /xxx
    {
        $result = $this->authenticationService->updateCredential($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/authe/credentials/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/edit.html.twig',
            $result
        );
    }

    public function deleteCredential(Request $request, Response $response, string $id): Response // (POST) /xxx
    {
        $result = $this->authenticationService->deleteCredential($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/authe/credentials/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/remove.html.twig',
            ['id' => $id]
        );
    }

    public function subscribeCredential(Request $request, Response $response): Response
    {//subscription form, sending activation link
        $result = $this->authenticationService->createCredential($request);
        if ($result === null) {
            return $this->render(
                $response,
                '@module/message.html.twig'
            );
        }

        return $this->render(
            $response,
            '@module/subscribe.html.twig',
            $result
        );
    }

    public function activateCredential(Response $response, string $id, string $activationIdHash): Response
    {//activation link checking
        $result = $this->authenticationService->activateCredential($id, $activationIdHash);
        if ($result === null) {
            return $this->render(
                $response,
                '@module/message.html.twig'
            );
        }

        return $this->render(
            $response,
            '@module/message.html.twig'
        );
    }

    public function logInCredential(Request $request, Response $response): Response
    {
        $result = $this->authenticationService->loginCredential($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                $request->getAttribute('previous_uri_path')
            );
        }

        return $this->render(
            $response,
            '@module/login.html.twig',
            $result
        );
    }

    public function restoreCredential(Request $request, Response $response): Response
    {//form with email, setting resetId and sending reset link
        $result = $this->authenticationService->restoreCredential($request);
        if ($result === null) {
            return $this->render(
                $response,
                '@module/message.html.twig'
            );
        }

        return $this->render(
            $response,
            '@module/restore.html.twig',
            $result
        );
    }

    public function resetCredential(Response $response, string $id, string $resetIdHash): Response
    {//reset link checking and redirecting to form restate password or back to restore procedure
        $result = $this->authenticationService->resetCredential($id, $resetIdHash);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/authe/restate'
            );
        }

        return $this->render(
            $response,
            '@module/message.html.twig'
        );
    }

    public function restateCredential(Request $request, Response $response): Response
    {//restate password form and redirect to login
        $result = $this->authenticationService->restateCredential($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/authe/login'
            );
        }

        return $this->render(
            $response,
            '@module/restate.html.twig'
        );
    }

    public function updateCredential(Request $request, Response $response): Response
    {
        $result = $this->authenticationService->updateCredential($request);
        if ($result === null) {
            return $this->render(
                $response,
                '@module/message.html.twig'
            );
        }

        return $this->render(
            $response,
            '@module/update.html.twig',
            $result
        );
    }

    public function unsubscribeCredential(Request $request, Response $response): Response
    {
        $result = $this->authenticationService->deleteCredential($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/'
            );
        }

        return $this->render(
            $response,
            '@module/unsubscribe.html.twig'
        );
    }

    public function logoutCredential(Request $request, Response $response): Response
    {
        $result = $this->authenticationService->logoutCredential($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/'
            );
        }

        return $this->render(
            $response,
            '@module/logout.html.twig'
        );
    }


    public function authenticate(Request $request, Response $response): Response
    {
        $result = $this->authenticationService->authenticate($request);
        if ($result === 303) {
            $payload = $this->authenticationService->getPayload();

            return $this->redirect(
                $response,
                '/autho/authorize?' . http_build_query($payload),
                $result
            );
        }

        return $this->render(
            $response,
            '@module/authenticate.html.twig',
            $this->authenticationService->getPayload()
        );
    }
}
