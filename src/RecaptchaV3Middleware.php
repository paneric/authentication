<?php

declare(strict_types=1);

namespace Paneric\Authentication;

use JsonException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Paneric\Interfaces\Session\SessionInterface;

class RecaptchaV3Middleware implements MiddlewareInterface
{
    private $session;
    private $config;

    public function __construct(SessionInterface $session, array $config)
    {
        $this->session = $session;
        $this->config = $config;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $this->session->setFlash(
            [
                'recaptcha_site_key' => $this->config['site_key'],
                'recaptcha_api_script' => sprintf($this->config['url_api_script'], $this->config['site_key']),
            ],
            'value'
        );

        if ($request->getMethod() === 'POST') {
            $captcha = $this->getCaptcha($request->getParsedBody()['g-recaptcha-response']);

            $request = $request->withAttribute('captcha', $this->verifyCaptcha($captcha));
        }

        return $handler->handle($request);
    }

    private function getCaptcha(string $gRecaptchaResponse): array
    {
        try {
            $captcha = file_get_contents(
                sprintf(
                    $this->config['url_site_verify'],
                    $this->config['non_ascii_key'],
                    $gRecaptchaResponse
                )
            );

            return json_decode($captcha, true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            echo sprintf(
                '%s%s%s%s',
                $e->getFile() . "\n",
                $e->getLine() . "\n",
                $e->getMessage() . "\n",
                $e->getTraceAsString() . "\n"
            );
        }

        return [];
    }

    private function verifyCaptcha(array $captcha): array
    {
        if ($captcha['success'] === false) {
            $this->session->setFlash($captcha['error-codes'], 'error');

            return $captcha;
        }

        if (isset($captcha['score']) && $captcha['score'] < $this->config['score']) {
            $captcha['success'] = false;

            $this->session->setFlash(['invalid_security_score'], 'error');
        }

        return $captcha;
    }
}
