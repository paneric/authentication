<?php

declare(strict_types=1);

namespace Paneric\Authentication\Service;

use Paneric\Interfaces\Guard\GuardInterface;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

class MailerService
{
    private $guard;
    private $mailer;
    private $config;
    private $local;

    public function __construct(
        GuardInterface $guard,
        PHPMailer $mailer,
        array $config,
        string $local
    ) {
        $this->guard = $guard;
        $this->mailer = $mailer;
        $this->config = $config;
        $this->local = $local;
    }

    public function setProcessId(): string
    {
        return $this->guard->generateRandomString($this->config['random_string_length']);
    }

    public function sendProcessLink(string $process, string $processId, string $userId, string $email): ?array
    {
        $messages = $this->config['mail_content'];

        try {
            $this->mailer->addAddress($email);
            $this->mailer->Subject = $messages[$process . '_email_subject'][$this->local];
            $this->mailer->Body = sprintf(
                $messages[$process . '_email_message'][$this->local],
                $this->config[$process . '_time_delay'],
                $this->config[$process . '_url'],
                $userId,
                $this->guard->hash($processId)
            );

            if ($this->mailer->send() === true) {
                return null;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return [];
    }
}
