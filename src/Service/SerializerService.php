<?php

declare(strict_types=1);

namespace Paneric\Authentication\Service;

class SerializerService
{
    public function jsonSerializeObjects(array $collection, bool $isCoc = false): ?array
    {
        if ($isCoc === false) {
            foreach ($collection as $key => $object) {
                $collection[$key] = is_array($object) ? $object : $object->convert();
            }

            return $collection;
        }

        foreach ($collection as $keyC => $objects) {
            $objects = $objects->convert();

            foreach ($objects as $key => $object) {
                $objects[$key] = is_array($object) ? $object : $object->convert();
            }

            $collection[$keyC] = $objects;
        }

        return $collection;
    }

    public function jsonSerializeObjectsById(array $objects): ?array
    {
        $objectsById = [];

        foreach ($objects as $object) {
            $key = is_array($object) ? $object['id'] : $object->getId();
            $objectsById[$key] = is_array($object) ? $object : $object->convert();
        }

        return $objectsById;
    }

    public function jsonSerializeArrays(array $attributes, String $class): array
    {
        $attributes = (array_filter($attributes, static function($attribute)
        {
            return is_array($attribute);
        }));

        $keys = array_keys($attributes);

        $values = [];
        $serializedValues = [];

        $i = -1;
        foreach ($attributes[$keys[0]] as $index => $attribute) {
            $i++;

            foreach ($keys as $key) {
                $values[$key] = $attributes[$key][$index];
            }

            $hydrator = new $class();

            $serializedValues[$i] = $hydrator->hydrate($values)->convert();
        }

        return $serializedValues;
    }

    protected function encodeFileName(string $fileName): string
    {
        $fileName = str_replace('.', '%', $fileName);

        return urlencode($fileName);
    }

    protected function decodeFileName(string $fileName, string $extension = null): string
    {
        $fileName = str_replace('%', '.', $fileName);

        $fileName = urldecode($fileName);

        return $extension === null ? $fileName : $fileName . $extension;
    }
}
