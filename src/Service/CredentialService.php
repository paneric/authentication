<?php

declare(strict_types=1);

namespace Paneric\Authentication\Service;

use Paneric\Authentication\DBAL\CredentialDTO;
use Paneric\Authentication\DBAL\CredentialRepositoryInterface;
use Paneric\Interfaces\DataObject\DataObjectInterface;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class CredentialService
{
    private $credentialRepository;
    private $session;
    private $serializer;
    private $guard;
    private $config;

    private $id;
    private $ref;
    private $payload;

    public function __construct(
        CredentialRepositoryInterface $credentialRepository,
        SessionInterface $session,
        SerializerService $serializer,
        GuardInterface $guard,
        array $config
    ) {
        $this->credentialRepository = $credentialRepository;
        $this->session = $session;
        $this->serializer = $serializer;
        $this->guard = $guard;
        $this->config = $config;
    }

    public function getAllPaginated(): array
    {
        $this->session->setFlash(['page_title' => 'content_credentials_show_title'], 'value');

        $pagination = $this->session->getData('pagination');
        $credentials= $this->credentialRepository->findBy(
            [],
            ['crd_ref' => 'ASC'],
            $pagination['limit'],
            $pagination['offset']
        );

        return [
            'credentials' => $this->serializer->jsonSerializeObjects($credentials)
        ];
    }
    
    public function getOneById(string $id): ?DataObjectInterface
    {
        return $this->credentialRepository->findOneBy(['crd_id' => $id]);
    }

    public function checkOneBy(Request $request, bool $isOAUTH = null): ?array
    {
        $validation = [];

        $attributes = $request->getParsedBody();

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[CredentialDTO::class];

            if (empty($validation)) {
                $credential = $this->credentialRepository->findOneBy(['crd_ref' => $attributes['ref']]);

                if ($credential === null) {
                    $this->session->setFlash(['db_credentials_login_error'], 'error');

                    return [];
                }

                if (!$credential->getIsActive()) {
                    $this->session->setFlash(['db_credentials_activation_error'], 'error');

                    return [];
                }

                $credentialConverted = $credential->convert();

                if (!$this->guard->verifyPassword($attributes['password_hash'], $credentialConverted['password_hash'])) {
                    $this->session->setFlash(['db_credentials_login_error'], 'error');

                    return [];
                }

                if (!$isOAUTH) {
                    $this->session->setData($credentialConverted, 'authentication');

                    return null;
                }

                $this->payload = $credentialConverted;
                
                return null;
            }
        }

        $credentialDTO = new CredentialDTO();

        return [
            'credential' => $credentialDTO->hydrate($attributes),
            'validation' => $validation
        ];
    }

    public function restoreOneBy(Request $request, string $resetId = null): ?array
    {
        $attributes = [];
        $validation = [];

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[CredentialDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $hydrator = $this->credentialRepository->findOneBy(['crd_ref' => $attributes['ref']]);

                if ($hydrator !== null) {
                    $attributes = $hydrator->convert();

                    $this->id = $attributes['id'];
                    $this->ref = $attributes['ref'];

                    $credential = new CredentialDTO();
                    $credential->setResetId($resetId);

                    $rowCount = $this->credentialRepository->update(
                        ['crd_id' => $this->id],
                        $credential
                    );

                    if ($rowCount > 0) {
                        return null;
                    }
                }

                $this->session->setFlash(['db_credentials_restore_error'], 'error');
            }
        }

        $credentialDTO = new CredentialDTO();

        return [
            'credential' => $credentialDTO->hydrate($attributes),
            'validation' => $validation
        ];
    }

    public function create(Request $request, bool $subscribeMode, string $id = null, string $activationId = null): ?array
    {
        $attributes = [];
        $validation = [];

        $credentialDTO = new CredentialDTO();

        if ($request->getMethod() === 'POST') {

            $attributes = $request->getParsedBody();

            $captcha = $request->getAttribute('captcha');

            if ($captcha['success'] === false) {
                return [
                    'credential' => $credentialDTO->hydrate($attributes),
                    'validation' => $validation
                ];
            }

            $validation = $request->getAttribute('validation')[CredentialDTO::class];

            if (empty($validation)) {

                if ($this->credentialRepository->findOneBy(['crd_ref' => $attributes['ref']]) === null) {
                    $this->ref = $attributes['ref'];

                    if ($subscribeMode) {
                        $attributes['role_id'] = $this->config['role_id'];
                        $attributes['activation_id'] = $activationId;
                    }

                    if (!$subscribeMode) {
                        $attributes['is_active'] = 1;
                    }

                    $attributes['id'] = $id;
                    unset($attributes['password_hash_repeat']);
                    $attributes['password_hash'] = $this->guard->hashPassword($attributes['password_hash']);

                    $credentialDTO->hydrate($attributes);

                    $lastInsertId = $this->credentialRepository->create($credentialDTO);

                    if ($lastInsertId === '0') {
                        return null;
                    }
                }

                $this->session->setFlash(['db_credentials_subscribe_error'], 'error');
            }
        }

        return [
            'credential' => $credentialDTO->hydrate($attributes),
            'validation' => $validation
        ];
    }

    public function update(Request $request, string $id): ?array
    {
        $validation = [];

        $credentialDTO = new CredentialDTO();

        $method = $request->getMethod();

        if ($method === 'POST') {

            $validation = $request->getAttribute('validation')[CredentialDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                if (isset($attributes['ref'])) {

                    if (!empty($this->credentialRepository->findByEnhanced(
                        ['crd_ref' => $attributes['ref']]
                        , 'OR',
                        $id
                    ))) {
                        $this->session->setFlash(['db_credentials_update_error'], 'error');

                        return [
                            'credential' => $this->credentialRepository->findOneBy(['crd_id' => $id]),
                            'validation' => $validation
                        ];
                    }
                }

                if (isset($attributes['password_hash'])) {
                    $attributes['password_hash'] = $this->guard->hashPassword($attributes['password_hash']);
                }

                $credentialDTO->hydrate($attributes);

                if ($this->credentialRepository->update(['crd_id' => $id], $credentialDTO) > 0) {
                    return null;
                }
            }
        }

        return [
            'credential' => $this->credentialRepository->findOneBy(['crd_id' => $id]),
            'validation' => $validation
        ];
    }

    public function delete(Request $request, string $id): ?array
    {
        if ($request->getMethod() === 'POST') {

            if ($this->credentialRepository->delete(['crd_id' => $id]) > 0) {
                return null;
            }

            $this->session->setFlash(['db_credentials_delete_error'], 'error');

            return [];
        }

        $this->session->setFlash(['page_title' => 'form_credential_remove_title'], 'value');
        $this->session->setFlash(['credential_delete_warning'], 'warning');

        return [];
    }

    public function updateStatic(string $id, DataObjectInterface $hydrator): ?array
    {
        if ($this->credentialRepository->update(['crd_id' => $id], $hydrator) > 0) {
            return null;
        }

        return [];
    }

    public function deleteStatic(string $id): ?array
    {
        if ($this->credentialRepository->delete(['crd_id' => $id]) > 0) {
            return null;
        }

        return [];
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function getPayload(): ?string
    {
        return $this->payload;
    }    
}
