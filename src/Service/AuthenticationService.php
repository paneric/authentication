<?php

declare(strict_types=1);

namespace Paneric\Authentication\Service;

use DateInterval;
use DateTimeImmutable;
use Exception;
use Paneric\Authentication\DBAL\CredentialDTO;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class AuthenticationService
{
    private $mailerService;
    private $credentialService;
    private $guard;
    private $session;
    private $payload;

    public function __construct(
        MailerService $mailerService,
        CredentialService $credentialService,
        GuardInterface $guard,
        SessionInterface $session
    ) {
        $this->mailerService = $mailerService;
        $this->credentialService = $credentialService;
        $this->guard = $guard;
        $this->session = $session;
    }

    
    public function getAllCredentials(): array
    {
        return $this->credentialService->getAllPaginated();
    }
    
    public function createCredential(Request $request, bool $subscribeMode = true): ?array
    {
        $id = null;
        $activationId = null;

        if ($request->getMethod() === 'POST') {
            $id = $this->guard->setUniqueId();
            
            if ($subscribeMode) {
                $activationId = $this->mailerService->setProcessId();
            }
        }

        $result = $this->credentialService->create($request, $subscribeMode, $id, $activationId);
        
        if (!$subscribeMode) {
            return $result;
        }
        
        if ($result === null) {

            $sendActivationLinkResult = $this->mailerService->sendProcessLink(
                'activation',
                $activationId,
                str_replace('.','_', $id),
                $this->credentialService->getRef()
            );

            if ($sendActivationLinkResult === null) {
                $this->session->setFlash(['credentials_create_success'], 'success');
                $this->session->setFlash(['credentials_activate_info'], 'info');

                return null;
            }

            $this->credentialService->delete($request, $id);

            $this->session->setFlash(['credentials_create_error'], 'error');

            return $sendActivationLinkResult;
        }

        return $result;
    }

    public function activateCredential(string $id, string $hash): ?array
    {
        $id = str_replace('_','.',$id);

        $credential = $this->credentialService->getOneById($id);

        if ($credential === null) {
            return [];
        }

        $attributes = $credential->convert();

        if (isset($attributes['is_active']) && $attributes['is_active'] === 1) {
            $this->session->setFlash(['credentials_activate_success'], 'success');

            return null;
        }

        $dateTimeImmutable = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $attributes['created_at']);

        if (!$this->checkTimeDelay($dateTimeImmutable, 24)) {
            $this->credentialService->deleteStatic($id);

            $this->session->setFlash(['credentials_activate_error'], 'error');
            $this->session->setFlash(['credentials_create_info'], 'info');

            return [];
        }

        $activationIdHash = $this->guard->hash($attributes['activation_id']);

        if (!$this->guard->verifyHash($activationIdHash, $hash)) {
            $this->session->setFlash(['credentials_activate_error'], 'error');
            $this->session->setFlash(['credentials_create_info'], 'info');

            return [];
        }

        $attributes['is_active'] = 1;

        $credential = new CredentialDTO;
        $credential->hydrate($attributes);

        if ($this->credentialService->updateStatic($id, $credential) === null) {
            $this->session->setFlash(['credentials_activate_success'], 'success');

            return null;
        }

        $this->session->setFlash(['credentials_activate_error'], 'error');
        $this->session->setFlash(['credentials_create_info'], 'info');

        return [];
    }

    public function loginCredential(Request $request): ?array
    {
        $result = $this->credentialService->checkOneBy($request);

        if ($result === null) {
            return $result;
        }

        return $result;
    }

    public function restoreCredential(Request $request): ?array
    {
        $resetId = null;

        if ($request->getMethod() === 'POST') {
            $resetId = $this->mailerService->setProcessId();
        }

        $result = $this->credentialService->restoreOneBy($request, $resetId);

        if ($result === null) {

            $sendResetLinkResult = $this->mailerService->sendProcessLink(
                'reset',
                $resetId,
                str_replace('.','_', $this->credentialService->getId()),
                $this->credentialService->getRef()
            );

            if ($sendResetLinkResult === null) {
                $this->session->setFlash(['credentials_restore_success'], 'success');
                $this->session->setFlash(['credentials_restore_info'], 'info');

                return null;
            }

            return $sendResetLinkResult;
        }

        return $result;
    }

    public function resetCredential(string $id, string $hash): ?array
    {
        $id = str_replace('_','.', $id);

        $credential = $this->credentialService->getOneById($id);

        if ($credential === null) {
            $this->session->setFlash(['credentials_reset_error'], 'error');
            $this->session->setFlash(['credentials_reset_info'], 'info');

            return [];
        }

        $attributes = $credential->convert();

        $dateTimeImmutable = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $attributes['updated_at']);

        if (!$this->checkTimeDelay($dateTimeImmutable, 24)) {
            $this->session->setFlash(['credentials_reset_error'], 'error');
            $this->session->setFlash(['credentials_reset_info'], 'info');
            return [];
        }

        $resetIdHash = $this->guard->hash($attributes['reset_id']);

        if (!$this->guard->verifyHash($resetIdHash, $hash)) {
            $this->session->setFlash(['credentials_reset_error'], 'error');
            $this->session->setFlash(['credentials_reset_info'], 'info');

            return [];
        }

        $this->session->setData(['id' => $id, 'hash' => $hash], 'credential_restate');

        return null;
    }

    public function restateCredential(Request $request): ?array
    {
        $id = $this->session->getData('credential_restate')['id'];
        $hash = $this->session->getData('credential_restate')['hash'];

        $credential = $this->credentialService->getOneById($id);

        if ($credential === null) {
            return [];
        }

        $attributes = $credential->convert();

        $resetIdHash = $this->guard->hash($attributes['reset_id']);

        if (!$this->guard->verifyHash($resetIdHash, $hash)) {
            return [];
        }

        $result = $this->credentialService->update($request, $id);

        if ($result === null) {
            $this->session->unsetData();

            return null;
        }

        return $result;
    }

    public function updateCredential(Request $request, string $id = null): ?array
    {
        if ($id !== null) {
            $id = str_replace( '_' ,'.' , $id);
        }

        if ($id === null) {
            $id = $this->session->getData('authentication')['id'];            
        }

        if ($this->credentialService->update($request, $id) === null) {
            $credential = $this->credentialService->getOneById($id);

            $this->session->setData($credential->convert(), 'authentication');

            $this->session->setFlash(['credentials_update_success'], 'success');

            return null;
        }

        return [];
    }

    public function deleteCredential(Request $request, string $id = null): ?array
    {
        if ($request->getMethod() === 'POST') {
            
            if ($id !== null) {
                $id = str_replace( '_' ,'.' , $id);
            }
            
            if ($id === null) {
                $id = $this->session->getData('authentication')['id'];

                $this->session->unsetData();
            }
            
            $this->credentialService->deleteStatic($id);
            
            return null;
        }

        if ($id !== null) {
            $this->session->setFlash(['credentials_delete_warning'], 'warning');
        }

        if ($id === null) {
            $this->session->setFlash(['credentials_unsubscribe_warning'], 'warning');
        }
        
        return [];
    }

    public function logoutCredential(Request $request): ?array
    {
        if ($request->getMethod() === 'POST') {
            $this->session->unsetData();

            return null;
        }

        $this->session->setFlash(['credentials_logout_warning'], 'warning');

        return [];
    }

    private function checkTimeDelay(DateTimeImmutable $referenceTime, int $timeDelay): bool
    {
        try {
            $currentTime = new DateTimeImmutable('now');
            $referenceTime = $referenceTime->add(new DateInterval('PT' . $timeDelay . 'H'));

            return $currentTime < $referenceTime;
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return false;
    }


    public function authenticate(Request $request): ?int
    {
        $result = $this->credentialService->checkOneBy($request, true);

        if ($result === null) {
            parse_str($request->getUri()->getQuery(), $queryParams);
            
            $this->payload = $queryParams;
            
            return 303; // REDIRECTION: 303 See Other
        }
        
        $this->payload = $result;
        
        return 200;
    }

    public function getPayload(): array
    {
        return $this->payload;
    }
}
