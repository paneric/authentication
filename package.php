<?php

declare(strict_types=1);

use DI\Container;
use Paneric\Authentication\RecaptchaV3Middleware as RecaptchaMiddleware;
use Paneric\Authentication\DBAL\CredentialRepository;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\Mailer\MailerFactory;
use Paneric\DBAL\Manager;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\Authentication\Service\AuthenticationService;
use Paneric\Authentication\Service\MailerService;
use Paneric\Authentication\Service\CredentialService;
use Paneric\Authentication\Service\SerializerService;

return [
    RecaptchaMiddleware::class => static function (Container $container): RecaptchaMiddleware
    {
        return new RecaptchaMiddleware(
            $container->get(SessionInterface::class),
            $container->get('recaptcha-middleware')
        );
    },

    CredentialRepository::class => static function (Container $container): CredentialRepository
    {
        return new CredentialRepository($container->get(Manager::class));
    },

    MailerService::class => static function (Container $container): MailerService
    {
        $mailerFactory = new MailerFactory();

        return new MailerService(
            $container->get(GuardInterface::class),
            $mailerFactory->create($container->get('mailer')),
            $container->get('mailer-service'),
            (string) $container->get('local')
        );
    },

    CredentialService::class => static function (Container $container): CredentialService {
        return new CredentialService(
            $container->get(CredentialRepository::class),
            $container->get(SessionInterface::class),
            $container->get(SerializerService::class),
            $container->get(GuardInterface::class),
            $container->get('credential-service')
        );
    },

    AuthenticationService::class => static function (Container $container): AuthenticationService {
        return new AuthenticationService(
            $container->get(MailerService::class),
            $container->get(CredentialService::class),
            $container->get(GuardInterface::class),
            $container->get(SessionInterface::class)
        );
    },
];
