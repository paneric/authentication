<?php

return [
    /* common */

    'main' => 'Main page',
    'authentication' => 'Authentication',
    'start' => 'Start',
    'credential' => 'Credential',
    'credentials' => 'Credentials',

    'email_address' => 'Email address',
    'email_address_help' => 'Your email will never be transferred to any third parties',
    'password' => 'Password',
    'password_help' => 'Your password must be min 8 characters long, contain at least one uppercase letter, number and special character',

    'role_id' => 'Role ID',
    'role_id_help' => 'Integer value of a role identifier',

    'cancel' => 'Cancel',
    'add' => 'Add',
    'confirm' => 'Confirm',
    'modify' => 'Modify',
    'update' => 'Update',
    'delete' => 'Delete',
    'restore' => 'Restore',
    'choose' => 'Choose...',
    'back' => 'Back',

    /* 'authe.credentials.show' */

    'content_credentials_show_title' => 'Credentials',
    
    /* authe.credential.subscribe */

    'form_subscribe_title' => 'Subscribe',
    'repeat_password' => 'Repeat password',
    'i_accept' => 'I accept',
    'i_accept_terms' => 'Herby, you confirm your acceptance for our Terms & Conditions',
    'subscribe' => 'Subscribe',

    'credentials_create_success' =>'Your account has been successfully created.',
    'credentials_activate_info' => 'Please check your email to activate your account by clicking the activation link.',

    'db_credentials_subscribe_error' => 'The entered email address is already in use.',

    /* authe.credential.subscribe - recaptcha*/

    'invalid_security_score' => 'Security score not sufficient.',
    'missing-input-secret' => 'The secret parameter is missing.',
    'invalid-input-secret' => 'The secret parameter is invalid or malformed.',
    'missing-input-response' => 'The response parameter is missing.',
    'invalid-input-response' =>	'The response parameter is invalid or malformed.',
    'bad-request' => 'The request is invalid or malformed.',
    'timeout-or-duplicate' => 'The response is no longer valid: either is too old or has been used previously.',

    /* authe.credential.activate */

    'credentials_activate_success' => 'Your account has been successfully activated.',
    'credentials_activate_error' => 'Your account activation has failed.',
    'credentials_create_info' => 'Probably the activation delay is exceeded. Try to register once again.',

    /* authe.credential.login */

    'form_login_title' => 'Log in',
    'log_in' => 'Log in',

    'form_authenticate_title' => 'Authentication',
    'authenticate' => 'Authenticate',

    'db_credentials_activation_error' => 'Your account is not activated.',
    'db_credentials_login_error' => 'The entered email address or password is invalid.',

    /* authe.credential.logout */

    'form_logout_title' => 'Log out',

    'credentials_logout_warning' => 'Your are about to log out. Please confirm.',

    /* authe.credential.unsubscribe */

    'form_unsubscribe_title' => 'Unsubscribe',

    'credentials_unsubscribe_warning' => 'Your are about to unsubscribe. Please confirm.',

    /* authe.credential.restore */

    'form_restore_title' => 'Restore',

    'credentials_restore_success' => 'Your account has been successfully identified.',
    'credentials_restore_info' => 'Please check your email to reset your password by clicking the reset link.',

    'db_credentials_restore_error' => 'The entered email address is invalid.',

    /* authe.credential.reset */

    'credentials_reset_error' => 'Your password reset has failed.',
    'credentials_reset_info' => 'Probably the reset delay is exceeded. Try to restore once again.',

    /* authe.credential.restate */

    'form_restate_title' => 'Submit new password',
    'restate' => 'Submit',

    /* authe.credential.add */

    'form_add_title' => 'Add credential',

    /* authe.credential.update */

    'form_update_title' => 'Modify credential',
    'new_email' => 'New email',
    'new_password' => 'New password',
    'repeat_new_password' => 'Repeat new password',

    'credentials_update_success' => 'Your credentials has been successfully modified.',

    /* authe.credential.delete */

    'form_delete_title' => 'Delete credential',
    'credentials_delete_warning' => 'Your are about to delete a credential. Please confirm.',
];
